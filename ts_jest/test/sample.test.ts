import { hello } from '../src/sample';

describe("test hello function", () => {
  it("should say hello", () => {
    expect(hello("team")).toBe("Hello team!");
  });
});